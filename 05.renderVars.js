//Imprimir variables en una vista
//Para eso primero convierte el html en una cadena
var http = require('http'),
    fs = require('fs');

http.createServer(function(req,res){
    fs.readFile('./index.html',function(err,html){

        var html_string = html.toString();//Así convierto el html en string

        //Una vez convertido en cadena usamos la exprecion regular match para buscar lo que este entre llaves
        
        //variables es un arrays con todo lo que esta en llaves
        var variables = html_string.match(/[^\{\}]+(?=\})/g);//Con match y eso me busca que este entre llaves
        var nombre = 'Gabriel';

        //con un for recorremos variables 

        for (var i = variables.length-1; i >=0; i--){
            var value = eval(variables[i]); //con eval evalua el contenido como js
            
            //sustituyo el valor del array por la variable
            html_string =html_string.replace("{"+variables[i]+"}",value)
        }
            res.writeHead(200,{"Content-Type":"text/html"})
            res.write(html_string);
            res.end();
        })
    }).listen(8080);