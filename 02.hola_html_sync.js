//Como leer un archivo en NODEJS sincrono

const { readFileSync } = require('fs');

var http = require('http'),
    fs = require ('fs');

var html = fs.readFileSync('./index.html');//Recibe un path, por eso va la direccion del html

http.createServer(function(req,res){
    res.write(html); //Estamos escribiendo en el navegador la respuesta
    res.end();
}).listen(8080);