var express = require('express');

var app = express (); //Así lo usamos


app.set('view engine', 'jade'); //Para usar jade para la vista

//Para visualizar cuando uso la url, necesito el metodo get

//METODO Http

app.get('/', function(req,res){
    res.render('index'); 
});

//Asi accedo a un parametro especifico de la url
app.get('/:nombre', function(req,res){
    console.log(req.params.nombre);
    res.render('form',{nombre: req.params.nombre}); //
});

app.post('/', function(req,res){
    res.render('form')
})
app.listen(8080); //lo conecto con el puerto 8080