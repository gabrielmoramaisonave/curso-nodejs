var http = require ('http') //Así importo la libreria, con require

//Recibe dos parametros la funcion

//1° Objeto con La solicitud del usuario
//2° Objeto con response

//createServer es una funcion, que recibe como parametro otra funcion
var servidor = http.createServer(function(solicitud, respuesta){
    console.log('Recibimos una petición');
    respuesta.end('Hola Mundo');//Así cierro conexion, sino queda cargando siempre 
})

servidor.listen(8080); //Así lo coloco en la red para poder hacer peticiones