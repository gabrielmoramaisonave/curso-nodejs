var http = require('http'),
    fs = require('fs');

http.createServer(function(req,res){
    fs.readFile('./index.html',function(err,html){
         //writeHead imprime el encabezado
         //Recibe 3 parametros:
         // 1° statusCode ej:200 si esta ok
         // 2° statusMessage ej: mandamos que tipo de contenido
         // 3° headers
            res.writeHead(200,{"Content-Type":"application/json"})
            res.write(JSON.stringify({nombre: "Gabriel", username:"Gabriel"}));
            res.end();
        })
    }).listen(8080);