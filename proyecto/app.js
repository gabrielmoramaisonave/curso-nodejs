var express = require('express');
var bodyParser =require('body-parser');
var app = express (); //Así lo usamos
var mongoose = require("mongoose"); //Para la base de datos
var Schema =mongoose.Schema

mongoose.connect("mongodb://localhost(fotos");//Así hago la conexion

//Tabla para la base de datos(aca uso objetos por ser mongo)
var userSchemaJSON ={
    email:String,
    password:String
};

var user_schema = new Schema (userSchemaJSON);//Crea un objeto que se convierte en la estructura de la tabla

var User = mongoose.model("User",user_schema);//Aca creo el modelo

//Ponemos la ruta /public, para que no haya conflicto con los nombres

//Con este middlewares servimos archivos estaticos
app.use('/public',express.static('public'))//Aca pasamos como parametro el middlewares que querramos usar

//Middlewares de parser(para leer los parametros)

app.use(bodyParser.json()) //Para peticiones application/json
app.use(bodyParser.urlencoded({extended: true})) // Para hacer parser de muchas cosas

app.set('view engine', 'jade'); //Para usar jade para la vista

//Para visualizar cuando uso la url, necesito el metodo get
app.get('/', function(req,res){
    res.render('index');
});

app.get('/login', function(req,res){
    User.find(function(err,doc){
        console.log(doc);
        res.render('login');
    })
});
    


app.post('/users', function(req,res){
    var user = new User({email: req.body.email, password: req.body.pass}); //Creo el nuevo usuario con los datos

    user.save(function(){ //guardamos los datos
        res.send('Recibimos tus datos');
    });
    

})

app.listen(8080); //lo conecto con el puerto 8080