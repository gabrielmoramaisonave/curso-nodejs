//Esta es la funcion que va a trabajar con los parametros, así no hacemos spaghetti
function parse(req){
    var arreglo_parametros = [], parametros = {}; //Declaramos una variable que sea un array de parametros
    
    if(req.url.indexOf('?')>0){//buscamos ? que es lo que nos indentifica los parametros
        var url_data = req.url.split('?')//Aca dividimos el array a partir de ?
        var arreglo_parametros = url_data[1].split('&');//Aca dividimos los parametros que estan dentro del elemento 1
    };

     //Lo recorremos ahora para ir modificando las variables
     for (var i = arreglo_parametros.length-1; i>=0; i--){
        var parametro = arreglo_parametros[i]; //Aca guardamos los parametros
        var param_data=parametro.split('=');//Dividimos el parametro en el =, para tener por un lado el nombre del parametro y por el otro el valor
        parametros[param_data[0]] = param_data[1];
    };
    
    return parametros;
}

module.exports.parse = parse;//De esta forma lo exporto así puedo usarlo en otros codigos