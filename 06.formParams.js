//Imprimir parametros que vienen de un formulario

var http = require('http'),
    fs = require('fs'),
    parser = require('./07.paramsParser.js'), //Así importo lo que exporte en el otro codigo
    render = require('./08.renderView.js');
var p = parser.parse; //Guardo en una variable la funcion que estoy importando
var render= render.render;
http.createServer(function(req,res){

    if(req.url.indexOf('favicon.ico') > 0){return;} //Inspeccionamos la URL para sacar la de favicon en caso que este

    fs.readFile('./index.html',function(err,html){

        var html_string = html.toString();
        var parametros = p(req); //Aca uso la funcion que importe, porque es para saber cuales son los parametros

   
            res.writeHead(200,{"Content-Type":"text/html"})
            res.write(render(html_string, parametros));
            res.end();
        });
    }).listen(8080);