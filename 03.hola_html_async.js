//Como leer un archivo en NODEJS asincrono

const { readFileSync } = require('fs');

var http = require('http'),
    fs = require ('fs');

//Recibe un path, y el callback. Mientras se procesa continua con el resto
var html = fs.readFile('./index.html', function(err,html){
    http.createServer(function(req,res){
        res.write(html); //Estamos escribiendo en el navegador la respuesta
        res.end();
    }).listen(8080);
});//Lee el archivo y cuando esta listo llama al callback

//Otra de las formas es metiendo el var html dentro del http.createServer
//Ventaja = No tengo que reiniciar el servidor para ver las modificaciones en el html
//Desventaja = Tiene que cargar el html cada vez que haga una peticion