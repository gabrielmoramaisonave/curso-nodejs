var express = require('express');

var app = express (); //Así lo usamos


app.set('view engine', 'jade'); //Para usar jade para la vista

//Para visualizar cuando uso la url, necesito el metodo get
app.get('/', function(req,res){
    res.render('index', {hola: 'Hola Gabriel'}); //puede ir index solo, o acompañado de una variable
    //el != en jade siginifica que imprime una variabel
    //el send, tambien termina la peticion. 
    //No especifico la extension, porque arriba defini que el motor era jade
    //entonces me va a buscar el index.jade
});

app.listen(8080); //lo conecto con el puerto 8080